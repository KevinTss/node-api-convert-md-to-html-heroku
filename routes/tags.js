const tagsRoutes = (app, fs) => {
  const dataPath = `./public/data/tags.json`;
  app.get("/tags", (req, res) => {
    fs.readFile(dataPath, "utf8", (err, data) => {
      if (err) {
        throw err;
      }

      res.send(JSON.parse(data));
    });
  });
};

module.exports = tagsRoutes;
