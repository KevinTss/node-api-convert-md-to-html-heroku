const showdown = require("showdown");
converter = new showdown.Converter();
const jsonDataPath = "./public/data/data-cards.json";
const directoryPath = "./public/data/recherches";

const cardsRoutes = (app, fs) => {
  let cards;

  const convertAndAddCard = (data, filename, id) => {
    content = converter.makeHtml(data);
    const title = filename.slice(0, -3);
    const slug = `card${id}`;
    const tags = [
      "bleu",
      "memoire",
      "ressources",
      "brouillon",
      "fiche lecture",
    ];
    cards.push({
      id,
      title,
      content,
      tags,
      slug,
    });
    writeJsonDataFile();
  };
  const writeJsonDataFile = () => {
    let json = JSON.stringify(cards);
    return new Promise((resolve, reject) => {
      fs.writeFile(jsonDataPath, json, "utf8", (err) => {
        (err) => {
          if (err) {
            return reject(err);
          } else {
            return resolve(data);
          }
        };
      });
    });
  };
  const readFiles = (filename, id) => {
    const dataPath = `./public/data/recherches/${filename}`;
    // let isFile;
    // fs.stat(dataPath, (err, stats) => {});
    fs.readFile(dataPath, "utf8", (err, data) => {
      if (err) {
        throw err;
      }
      convertAndAddCard(data, filename, id);
    });
  };
  const readDirectory = () => {
    cards = [];
    return new Promise((resolve, reject) => {
      fs.readdir(directoryPath, function (err, files) {
        if (err) {
          return reject(err);
        } else {
          files.forEach((file, id) => {
            readFiles(file, id);
          });
          return resolve(true);
        }
      });
    });
  };

  const sendJsonFile = (res) => {
    fs.readFile(jsonDataPath, "utf8", (err, data) => {
      if (err) {
        throw err;
      }
      console.log("read", JSON.parse(data));
      res.send(JSON.parse(data));
    });
  };

  readDirectory();

  app.get("/cards", (req, res) => {
    sendJsonFile(res);
  });

  app.post("/cards", (req, res) => {
    //CREATE AND WRITE MD FILE
    const writeFilePromise = (data) => {
      return new Promise((resolve, reject) => {
        fs.writeFile(
          `${directoryPath}/${req.body.slug}.md`,
          req.body.content,
          "utf8",
          (err) => {
            if (err) {
              return reject(err);
            } else {
              return resolve(data);
            }
          }
        );
      });
    };

    let allPromise = Promise.all([writeFilePromise(), readDirectory()]);
    allPromise.then(sendJsonFile(res));

    // .then((result) => sendJsonFile(res))

    // fs.readFile(jsonDataPath, "utf8", (err, data) => {
    //   if (err) {
    //     throw err;
    //   }
    //   let obj = JSON.stringify(data);
    //   let create = false;
    //   obj.map((item) => {
    //     if (req.body.id === item.id) {
    //       return req.body;
    //     } else {
    //       create = true;
    //     }
    //   });
    //   if (create) {
    //     obj.push(req.body);
    //   }
    // });
  });
};

module.exports = cardsRoutes;
