const express = require("express");
const path = require("path");
const cors = require("cors");
const PORT = process.env.PORT || 5000;
const bodyParser = require("body-parser");
const fs = require("fs");

const app = express();
app
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(cors())
  .use(express.static(path.join(__dirname, "public")))
  .set("views", path.join(__dirname, "views"))
  .set("view engine", "ejs")
  .get("/favicon.ico", (req, res) => {
    // Use actual relative path to your .ico file here
    res.sendFile(path.resolve(__dirname, "../favicon.ico"));
  })
  .get("/", (req, res) => res.render("pages/index"))
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

const routes = require("./routes/routes")(app, fs);
