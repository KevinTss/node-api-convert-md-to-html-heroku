# Antoine Gelgon memoire
[lien vers site](https://github.com/Antoine-Gelgon/memoire-dnsep)

 ## note: 
Le temps de la vie active vs vie innactive et la technique qui permet l'activation quand elle n'est pas incomprise, obscure donc aliénante.

## tag:
- mémoire
- reférence
- design
- technique

## Extrait

![image conclusion mémoire](../images/antoine-gelgon-memoire-conclusion.jpg)

## to do
demander a Antoine accès pour lire le mémoire