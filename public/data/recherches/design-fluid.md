# Design Fluid mémoire Louise Drulhe
[lien vers site](https://louisedrulhe.fr/designfluide/)

 ## note: 
 Le travail de designer graphique avec ces nouvelles techno à du s'adapter et être repenser. 
 Question, qu'est ce que ça a changer?
 la question du temps dans nouveaux espaces.
       le designer graphique doit prendre en compte que le contenu n'est pas fixé mais évolue au fil du temps.

## tag:
- mémoire
- reférence
- design

## Extrait

![image citation](../images/citation-temps.png)

[gilles debrock extrait](https://www.ofluxo.net/liquid-design-the-new-public-space-by-gilles-de-brock/)
Zygmunt Bauman “[…] in simple language […] liquids, unlike solids, cannot easily hold their shape. Fluids, so to speak, neither fix space nor bind time. While solids have clear spatial dimensions but neutralize the impact, and thus downgrade the significance, of time (effectively resist its flow or render it irrelevant), fluids do not keep to any shape for long and are constantly r

eady (and prone) to change it; and so for them it is the flow of time that counts, more than the space they happen to occupy: that space, after all, they fill but ‘for a moment’.

