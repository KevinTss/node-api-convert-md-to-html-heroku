# INTRO
Who can wait quietly while the mud settles?
Who can remain still until the moment of action?
Laozi, Tao Te Ching

#### HISTOIRE
Aaron swartz dans un de ses articles nous dit que les nouvelles technologies deviennent rapidement si omniprésentes dans nos quotidiens qu'il est parfois difficile de se souvenir de comment les choses étaient avant.

#### NOTRE CERVEAU
La puissance du cerveau humain c'est sa capacité à créer du vide. C'est ce qui permet d'apprendre, de comprendre et d'imaginer. 
Dans son livre Médiarchie Yves citton explique que dans notre génération les nouveaux médiums nous accablent en permanence de stimulus, et en déclanchant chez nous  des besoins de réponses instantanées, ils nous empechent de créer le vide nécéssaire pour comprendre, annalyser et construire une réfléxion sur les informations reçus.

#### INVISIBLE (support et techno)
En règle générale on remarque que nos appareils de médiation tendent à rester invisibles ou transparents tant qu’ils fonctionnent selon ce que nous attendons d’eux.

#### AJAX ROUAGE
En éléminant les frictions sur les sites web du au temps de rafraichisement des pages à chaque nouvelle request au serveur, la technologie Ajax (Asynchronous javascript and XML) à grandement participé à créer ce rythme arrassant de boucles retroactives.

Pour mon mémoire je souhaite m'intérrésser à cette technologie qui a révolutionné le web tel que nous le connaissons aujourd'hui et continue de modeler l'avenir de nos navigation.
Elle est aussi en ce moment au coeur de l'actualité du dévellopement web car c'est sur elle que sont basés les frameworks frontend javascript de plus en plus largement utilisés.

<!-- #### INVISIBLE -->
<!-- Pourtant cette actualité et ces changement majeurs dans nos interfaces,  sont invisibles et  passent silencieusement prés des oreils des designers graphique. -->

#### HISTOIRE
 _Même si cette évolution peut s'embler anodine ou infime dans l'histoire du web, elle est aujourd'hui omniprésente dans nos quotidiens et je pense qu'il est intérréssant pour le designer d'interface, de web, et théoriciens de la novuelle communication de comprendre le fonctionnement de ces technologies, leurs histoire et comment elles ont pu sans crier gard modifier nos preception du temps et temporalité devant nos écrans._

#### LE TEMPS
**Pour mieux comprendre comment ces technologies opèrent sur nos expériences du temps dans les interfaces web**, je souhaite réaliser une série de courts programmes pour dans le but de mieux comprendre et éclairé sur les différents concepts parcouru dans mon mémoire.


**Les résultats de ces programmes seraient des formes et éxpériences d'interface singulières qui viendraient questionner nos rapport du temps devant nos écrans.
Aussi les formes générés pourraient elle même éclairer sur les étapes de la conception du programme, sur son architecture, et permettre visuellement de comprendre ses rouages et mécanismes interne.**

Pour construire ce mémoire je construit un outil qui me permet de penser des formes d'écriture... et qui lui donnera sa forme finale.

Avec l'actualité du coronavirus, la quarantaine, l'invisibilté de ces mécanisme qui passent dans nos vie et modifient notre quotidien insidueusement.
Comment ça s'immisce et qu'est ce que ça déplace?
<!-- ça s'immisce sans bruit, sans faire de vague et se demander au niveau des neurosciences ou en sont le dev. On sait comment trigger des trucs avec les image, captievr notre éttention génrer des sécrétion de dopamine. -->

 exemple c'est facebook qui a developpé react et c'est eux aussi cambridge analitica coincidence🤔🤯? 


Plan temporaire:
- histoire d'ajax 
- histoire de js
  - l'orienté objet
  - syntaxe, création dans urgence et dev d'un language web
  - histoire petite révolution sur le web
- neuroscience et effet du rafraichissemeent instantanné sur nos cerveaux
  <!-- - dans ton crane comme un virus -->
  - scroll habits
<!-- - politique et gafam monopole -->
- ux insidieux, une navigation orienté
- outil 
  <!-- - théorisien penseur sur reprendre l'outil pour la bataille! -->
- penser le design d'interface, pas juste dev qui créé impunément
- la typographie sur le web à l'heure d'ajax
<!-- - petit cours du web -->
- c'est planétaire ton téléphone entre les mains les mauvais choix sont plus faciles.



<!-- ## Brouillon

précisément l'objet de recherche.

Comment écrire un mémoire sur son mémoire,
Comment le language Javascript et les frameworks frontend peuvent ils
aider à libérer la création du designer graphique.

- création de formes singulière, nouvelles expèriences de lecture, une liberté de création et singularité. 
Les framework frontend javascript une esthétique singulière, 

- au départ des envies de formes et des programmes singuliers
- construction -> comment c'est fabriqué, expliquer chaque étape
- esthétique
- navigation
- outil utilisé
- structure du mémoire, organisation --> shématiser
- structure des textes inspiré par oop
- déconstruire les réflexes de navigation et code ux de startup (singer our permettre un conscientisation)
- proposer avec des interfaces expèrience singulière de lecture...
- philosophie du bricolage et poésie du programme
 -->

## ressources
### Markdown

Utilisation du language Markdown pour mettre en page le texte et collaborer sur pad.
Ce language a été créé en 2004 par John Gruber et Aaron Swartz et n’a pas évolué depuis. Même si de nombreuses extensions et “extras” sont venues se greffer au projet originel.

### UML et shématisation de programme (et db schema?)

2 parties pour début de conception du projet
rendre plus compréhensible des objets complexes,
première itération sur les plans de la vision extérieure et les plans de surface.

2em itération
Par la suite, l’architecte réalisera les plans techniques qui seront nécessaires pour les différents corps de métier (les maçons, les électriciens, les plombiers, etc.). Il arrive parfois que ces plans mettent en évidence certaines contraintes qui obligent l’architecte à revenir sur les plans de surface 

#### se demander le client il veut quoi?

Dans la phase d’analyse, on cherche d’abord à bien comprendre et à décrire de façon précise les besoins des utilisateurs ou des clients. Que souhaitent-ils faire avec le logiciel ? Quelles fonctionnalités veulent-ils ? Pour quel usage ? Comment l’action devrait-elle fonctionner ? 
Dans la phase de conception, on apporte plus de détails à la solution et on cherche à clarifier des aspects techniques, tels que l’installation des différentes parties logicielles à installer sur du matériel.
Umml Unified modeling language , inventer pour --> oop lien
une des notation les plus utilisés pour construire un logiciel?
phase d'analyse,,, 

![ulm schema exemple](../assets/md-images/uml-schema.png)
  
### La musique 

- **[La performativité du language](https://fr.wikipedia.org/wiki/Performativit%C3%A9)** 
  C'est le fait pour un signe linguistique (énoncé, phrase, verbe, etc.) d'être performatif, c'est-à-dire de réaliser lui-même ce qu'il énonce. Le fait d'utiliser un de ces signes fait alors advenir une réalité.
- informer un caractère de son propre comportement
- **éxecution d'un programme**
    - le navigateur
        https://web.developpez.com/tutoriels/web/how-browsers-work/
    - moteur de rendu
      Nos navigateurs de références Firefox, Chrome et Safari sont construits sur deux moteurs de rendu. Firefox utilise Gecko un moteur « fait maison » de Mozilla. Safari et Chrome utilisent Webkit.
      WebKit est un moteur de rendu Open Source qui a commencé comme un moteur de plateforme Linux et a été modifié par Apple pour soutenir Mac et Windows. 
      https://web.developpez.com/tutoriels/web/how-browsers-work

### OOP (programmation orienté objet) et comment décrire le monde

 Il consiste en la définition et l'interaction de briques logicielles appelées objets ; un objet représente un concept, une idée ou toute entité du monde physique, comme une voiture, une personne ou encore une page d'un livre. Il possède une structure interne et un comportement, et il sait interagir avec ses pairs. Il s'agit donc de représenter ces objets et leurs relations ; l'interaction entre les objets via leurs relations permet de concevoir et réaliser les fonctionnalités attendues, de mieux résoudre le ou les problèmes. Dès lors, l'étape de modélisation revêt une importance majeure et nécessaire pour la POO. C'est elle qui permet de transcrire les éléments du réel sous forme virtuelle. 

### créer ma palette d'outils

je vient d'une pratique de plasticienne, créer des outils pour faire du graphisme. Penser en code, en programme une architecture et une histoire. Une esthétique, un rythme une musicalité, une matière, du bricolage, de la poésie. Aller retour réel et web.
Juste des outils pour construire des chateaux de cartes.

### les frameworks frontend en Js
  - Aaron swartz et Ajax
  - pourquoi c'est trendy
  - Vue js 
  - React /n
React is the definite leader in the JS world. This JavaScript framework uses a reactive approach and also introduces many of its own concepts for front-end web development. 

### petite histoire de la syntaxe Javascript  

It would accept almost anything I typed but interpret it in a way that was completely different from what I meant. This had a lot to do with the fact that I did not have a clue what I was doing, of course, but there is a real issue here: JavaScript is ridiculously liberal in whatit allows. The idea behind this design was that it would make programming inJavaScript easier for beginners. In actuality, it mostly makes finding problemsin your programs harder because the system will not point them out to you.This flexibility also has its advantages, though. It leaves space for a lot oftechniques that are impossible in more rigid languages, and as you will see(for example inChapter 10), it can be used to overcome some of JavaScript’sshortcomings. 

## liste des projets

- **queer phenomenology et desorientation**
    - pb de diversité et mixité sur le web:
        - HYF , womencoding, et mon expérience dans l'incubateur de sf.
        - ressentit de forme d'un programme (c'est pas hermétique)/ceux qui le construisent (biais de genre)
    - Le baton de parole et Aaron Swartz
        - editaton pour femmes à Bx
        - Aujourd'hui tt le monde peut s'exprimmer sur le web la question est de savoir qui est entendu 
        - le mouvement des gilets jaunes sur fb ?
        - podcast sur philosophe dans la montagne
    

- **enginering care**
  
    - interview entrepreneur et dev de jeux vidéos pour maison de retraite
    - cynisme de startupeur
    - la gamification: non pas pour créer du lien sociale mais plutôt pour vous lier au produit
    - le Quantified Self connait toi toi même. Ton meilleur médecin c'est toi. L'avenir du millieu de la santé quand le patient se connait +++ 
    - PetRobot (actionnaire Inbev document) et mentalité Japonaise: les amats de techno qui font parties de l'environnement (ex: temagochie, loving game...) / la philosophie du bricolage cf camille Henrot
    - la forme du robot doit s'éloigner au max de celle d'un humain à cause de l'imaginaire collectif (les robots prennent le controle et nous remplacent)
    - Johny CAsh et l'amitié -denrée rare
   

- **mayXaySinhTo**  
   
    - description du voyage, du workshop et de l'expo
        - récits familiaux, ésthétique, histoire de colonisatio française en indochine
        - rencontre Tam et collaboration com pour l'expo
        - workshop de programmation pour les étudiants
        - dévellopement du site et vidéo exposé au musée 
        - organisation, montage de l'expo, choix scénographie...
        - rencontres et discutions avec directeur du musée d'HCMC et Université, (la censure)
        - découverte d'un pays système avec gouvernement communiste 
        - le delta en voie de disparition à cause des nappes fréatiques,
    - histoire de ma famille:
        - réactualiser, remettre en perspectiveles récits familiaux avec mon expèrience perso
        - ésthétique textile peinture paysage...

    - marguerite duras:
        - visite de la maison
        - marguerite et l'écriture  
        - interdiction de regarder le film à part dans l maison de l'amant
    - dévellopement de l'art contemporain au Vietnam :
        - éviter le regard éthnocentré
        - après 30 ans de guerre (pillage, destruciton, art en    Asie réduit à chinoiserie) un vent frais et une urgence de vivre.
        - récements les seins nues dans le musée d'HCMC cotoie les peintures traditionelles en lack
    

- **git et Raphael Bastide**
    - Linus torvald et l'utopie du web
    - git comment ça marche
    - revolution de la collaboration et bcp de potentiel pour d'autres secteurs
    
- **p2p**
    - enjeux socio politique des infrastructures réseaux
- **web histoire de l'utopie? arts num de l'époque...**


## Biblio

- E. H Gombrich "histoire de l'art"
- persuasion clandestine Vance Backard
- conf prepostprint
- eloquent js
- back office
- Anthony de Masure entretien petit banc
- Raphael Einthoven (inspi forme: courtes nouvelles sur sujet actualité)
- Simmondon parler de progra + technique.. 
<!-- - lionel aventure imprimmante -->
- Louise Drhule
- c'est une romance (cf Fleebag)
- hors normes ->une comédie populaire qui fait passer des messages politiques 
<!-- - site de la villa hermosa -->
- mémoire Antoine Gelgon,,Etienne Lozeret ..

## Methodo

- enregistrement audio
- raconter comment l'outils est construit étapes...
- comment collaborer sur --> markdown rules pour l'auteur 
- listes des branches et commit git/ gitlab open source?
- comment faire citations, ref...
- trouver le ton d'écriture 😂
- exo érciture : OOP pour écrire un texte template (activation du texte, boilerplate, contrainte)
- création de l'outil site mémoire
- ton: design pour site inspi (startup world parodie)
- taff de terrain, anthropologique (faire interviews, rencontres...)
- traduction en Anglais?



### methodo texte?

- copier coller texte ailleurs puis réécrire?
- remplir puis vider?
- Jul technique -> produire souvent pleins de petits textes pour écrire chapitres

#### dev 

- react ++ <3 for front ? connaisance construction vue.js , app node.. / comprehension js history.. syntaxe devellopement, ajax..
- python?


# Liens 

VUE
https://syntax.fm/show/130/the-vuejs-show-scott-teaches-wes
https://www.scotttolinski.com/projects
https://www.youtube.com/watch?v=rzK3v1SzdQQ
https://www.grafikart.fr/tutoriels/decouverte-809
https://www.udemy.com/course/vuejs-2-the-complete-guide/
MAximillian udemy
    
UML
[uml wikipedia](https://fr.wikipedia.org/wiki/UML_(informatique))
[uml openclassrooms](https://openclassrooms.com/fr/courses/2035826-debutez-lanalyse-logicielle-avec-uml/2048781-les-differents-types-de-diagrammes)
[uml-diagrams](https://www.uml-diagrams.org/sequence-diagrams.html)


MATOS INFORMATIQUE LOCATION 
https://www.ecologic.be/magasin/
http://www.culture.be/index.php?id=2117

MEMOIRE STEPHANIE
https://gitlab.constantvzw.org/lorarjohns/work.aesthetic-programming
https://generativeartistry.com/tutorials/hypnotic-squares/

DEV LIENS: 
https://www.primative.net/fr/blog/10-extensions-indispensable-pour-vs-code/

ESPACE EXPO JURY
https://www.instagram.com/elevensteens/
https://www.elevensteens.com/contact

INSPI
toledano et nacache
https://louisedrulhe.fr/designfluide/#intro
https://eloquentjavascript.net/11_async.html
http://www.aaronsw.com/weblog/ajaxhistory

typo
http://www.t-o-m-b-o-l-o.eu/page/5/
https://reshape.network/directory
  lire texte sur le numérique art mail renauld

  harrisson
  https://eatock.com/2004/my-favourite-cup/
  https://eatock.com/about/daniel-eatock/2/'

COurs florent orale
https://www.coursflorent.fr/checkout/revue/13134

http://www.fridamedrano.com/jabin.html?

site
https://www.fariskassim.com/
https://artpapereditions.org/product/book/ape087-michiel-arnout-de-cleene-f1-13/
[tiger.exposed](https://tiger.exposed/)
https://haegeman-temmerman.be/

